#include<iostream>

using namespace std;

class student
{
	protected:int roll;
	public:void get_roll(int);
		void put_roll(void);
};
void student::get_roll(int a)
{
	roll=a;
	cout<<"\nRoll number recorded";
}
void student::put_roll(void)
{
	cout<<"\nThe Roll number is : ";
	cout<<roll;
}
class test:public student
{
	protected:float s1,s2;
	public:void get_marks(float,float);
		void put_marks(void);
};
void test::get_marks(float a,float b)
{
	s1=a;
	s2=b;
	cout<<"\nMarks Recorded ";

}
void test ::put_marks(void)
{
	cout<<"\nThe marks are : \nSubject 1 : "<<s1<<"\nSubject 2 : "<<s2;
}
class result:public test
{
	float total;
	public:void get_result(void);
		void print_result(void);
};
void result::get_result(void)
{
	total=s1+s2;
	cout<<"\nTotal Calculated ";

}
void result ::print_result(void)
{
	cout<<"\nThe results is Total = "<<total;
}

int main()
{
	result std;
	cout<<"\nEnter Roll number : ";
	int roll_no;
	cin>>roll_no;
	std.get_roll(roll_no);
	cout<<"\nEnter the marks from 2 subjects : ";
	int sub1,sub2;
	cin>>sub1>>sub2;
	std.get_marks(sub1,sub2);
	cout<<"\nThe Details of student : \n";
	std.put_roll();
	std.put_marks();
	std.get_result();
	std.print_result();
	return 0;
}
