#include<iostream>

using namespace std;

class student
{
	protected:int roll;
	public:void get_roll(void)
		{
			cout<<"\nEnter the Roll number : ";
			cin>>roll;
		}
		void put_roll(void)
		{
			cout<<"\nRoll number : "<<roll;
		}
};
class test
{
	protected:float m1,m2;
	public:void get_marks(void)
		{
			cout<<"\nEnter the Marks of 2 subjects : ";
			cin>>m1>>m2;
		}
		void put_marks(void)
		{
			cout<<"\nThe marks are : \nMark 1 : "<<m1<<"\nMark 2 : "<<m2;

		}
};
class results : public student,public test
{
	float total;
	public:void print_results(void)
		{
			total=m1+m2;
			cout<<"\nThe total is : "<<total<<"\n";
		}
};
int main()
{
	results std;
	std.get_roll();
	std.get_marks();
	cout<<"\nDetails : ";
	std.put_roll();
	std.put_marks();
	std.print_results();
	return 0;
}
