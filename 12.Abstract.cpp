#include<iostream>
#include<stdlib.h>
using namespace std;

class Animal{
public:
    virtual void sound() = 0;

};
class Dog: public Animal{
public:
   void sound() {
      cout<<"Woof"<<endl;
  }
};
class Cat:public Animal{
public:
    void sound(){
	cout<<"Meow"<<endl;
   }
};
class Lion:public Animal{
public:
    void sound(){
	cout<<"Roar"<<endl;
  }
};
class Horse:public Animal{
public:
    void sound(){
	cout<<"Neigh"<<endl;
  }
};
int main(){

   Dog d;
   d.sound();
   Horse h;
   h.sound();
   Lion l;
   l.sound();
   Cat c;
   c.sound();
   return 0;

}
