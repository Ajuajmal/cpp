#include <iostream>
#include<stdlib.h>

using namespace std;

class Queue {
private:
    int item, i;
    int arr[100];
    int rear;
    int front;

public:

 Queue() {
     rear = 0;
     front = 0;
 }

 void insert() {
     if (rear == 100)
         cout << "\n Queue Reached Max!";
     else {
         cout << "\nEnter The Value to be Insert : ";
         cin>>item;
         cout << "\n Position : " << rear + 1 << " , Insert Value  : " << item;
         arr[rear++] = item;
     }
 }

 void remove() {
     if (front == rear)
         cout << "\n Queue is Empty!";
     else {
         cout << "\n Position : " << front+1 << " , Remove Value  :" << arr[front];
         front++;
     }
 }

 void display() {
     cout << "\n Queue Size : " << (rear - front);
     for (i = front; i < rear; i++)
         cout << "\n Position : " << i+1 << " , Value  : " << arr[i];
 }
};

int main() {
 int choice, ex= 1;
 Queue obj;
 do {
     cout << "\n\n Queue Main Menu";

     cout << "\n1.Insert \n2.Remove \n3.Display \nOthers. exit";
     cout << "\nEnter Your Choice : ";
     cin>>choice;
     switch (choice) {
         case 1:
             obj.insert();
             break;
         case 2:
             obj.remove();
             break;
         case 3:
             obj.display();
             break;
         default:
             ex= 0;
             break;
     }
 } while (ex);

 return 0;
}
